<?php
/*
Plugin Name: Privacy Button
Plugin URI: https://usercentrics.com/
Description: Privacy Button helps you to protect your page against breaches in privacy.It enables you to integrate a small Javascript on your page which will then ask your visitor for any consent that you decide you legally require for your website. That consent is then provided e.g. to your Tag Management System where you can integrate the logic of activating or deactivating specific technologies like an analytics toole, depending on your users consents.
Version: 1.0.1
Author: Usercentrics,GmbH
Author URI: https://usercentrics.com/
License: GPLv2 or later
Text Domain: usercentrics.com
*/
error_reporting(0);

if ( !function_exists( 'add_action' ) ) {
	
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
	
}

/*
* Function Creating necessary tables 
* for the Plugin
*/

function create_plugin_database_table() {
	
	global $wpdb;
	$table_name = $wpdb->prefix.'usercentrics';
	$sql        = "CREATE TABLE  $table_name (
	`id` int(11) NOT NULL UNIQUE KEY AUTO_INCREMENT,
	`instance` varchar(512) NOT NULL,
	`published` int(11) NOT NULL
	) ENGINE=MyISAM  DEFAULT CHARSET=utf8";
	$wpdb->query($sql);
	
}

register_activation_hook( __FILE__, 'create_plugin_database_table' );
add_action('admin_menu', 'usercentrics_plugin_setup_menu');
add_action( 'wp_ajax_save_setting_ID', 'save_setting_ID' );
add_action( 'wp_ajax_save_setting_status', 'save_setting_status' );

if (is_admin()) {
	
	wp_enqueue_script('mylib', plugin_dir_url(__FILE__) . 'js/iframe-api-button.js','',false,false);
	
	if( isset($_REQUEST['page']) == 'usercentrics-plugin')
	{   
        wp_enqueue_script('proper', plugin_dir_url(__FILE__) . 'js/popper.min.js','',false,false);
		wp_register_style( 'custom-style', plugins_url( '/css/custom-style.css', __FILE__ ), array(), '20120208', 'all' );
		wp_enqueue_style( 'custom-style' );
		wp_enqueue_script('bootstrap', plugin_dir_url(__FILE__) . 'js/bootstrap.min.js','',false,false);
		wp_register_style( 'bootstrap-style', plugins_url( '/css/bootstrap.min.css', __FILE__ ), array(), '20120208', 'all' );
		wp_enqueue_style( 'bootstrap-style' ); 
		wp_enqueue_script('bootstraptoggle', plugin_dir_url(__FILE__) . 'js/bootstrap-toggle.min.js','',false,false);
		wp_register_style( 'bootstrap-toggle', plugins_url( '/css/bootstrap-toggle.min.css', __FILE__ ), array(), '20120208', 'all' );
		wp_enqueue_style( 'bootstrap-toggle' );	
    
	}
}
add_action( 'wp_head', 'usercentricscode', 10 );

/*
* Function for showing 
* the plugin in wordpress frontend
*/

function usercentricscode() { 

	global  $wpdb;
	$qry = "SELECT instance,published FROM $wpdb->prefix" . 
	"usercentrics limit 1";
	$usercentrics = $wpdb->get_results( $qry );
	
	if(count($usercentrics)>0){
		
		$instance_value =	$usercentrics[0]->instance;
		$published      =	$usercentrics[0]->published;
	
	}
	
	if($published==1 && !is_admin()){
	
	?>
    <script>
	(function(w,d,s,l,i){
	  w[l]=w[l]||[];
		w[l].push({
		'gtm.start':
			new Date().getTime(),event:'gtm.js'}
        );
		var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';
		j.async=true;
		j.src=
		'https://button.usercentrics.eu/assets/main.js?id='+i+dl;
		f.parentNode.insertBefore(j,f);
		}
    )(window,document,'script','dataLayer','<?php echo $instance_value;?>');
    </script>
    <!-- End Google Tag Manager -->
    <?php 
    
	}
}

add_action( 'wp_head', 'usercentricscode', 10 );

function usercentrics_plugin_setup_menu(){
	
	add_menu_page( 'Usercentrics', 'Usercentrics', 'manage_options', 'usercentrics-plugin', 'usercentrics_init',plugins_url( 'images/icon.jpg', __FILE__ ));

}

/*
* Function for Enable or Disable 
* the plugin from backend
*/

function save_setting_status(){	
	
	global $wpdb;
	$status = $_REQUEST['status'];
	$table  = $wpdb->prefix."usercentrics";
	$sql    = "update $table set published=$status";
	$wpdb->query($sql);
	
	if($status == 1){
		
		echo "Plugin enabled Successfully";
		
	}elseif($status == 0){
		
		echo "Plugin disabled Successfully";
		
	}
	exit();
	
}

/*
* Function for getting the latest URL 
* of json data used in the Plugin.
*/

function get_redirect_target($url) {
    
	$ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $headers = curl_exec($ch);
    curl_close($ch);
    if (preg_match('/^Location: (.+)$/im', $headers, $matches))
        return trim($matches[1]);
    return $url;

}

/*
* Function for Saving the setting 
* ID for the Plugin.
*/

function save_setting_ID() {
	
	global $wpdb;
	$settingId = $_REQUEST['settingId'];
	$task = $_REQUEST['task'];
	
	if($task!=''){
		
		$table = $wpdb->prefix."usercentrics";
		$sql = "update $table set instance= '$settingId'";
		$wpdb->query($sql);
		
	}else{
		
		$table = $wpdb->prefix."usercentrics";
		$sql = "INSERT INTO $table (instance, published) VALUES ('$settingId',1)";
		$wpdb->query($sql);
		
	}
	exit();
}

/*
* Functions for the various 
* shortcodes of the plugin.
*/

function usercentrics_all_consents_full_shortcode() {
	
	return '<div class="usercentrics-all-consents-full" ></div>';

}

add_shortcode( 'usercentrics-all-consents-full', 'usercentrics_all_consents_full_shortcode' );

function usercentrics_single_consent_shortcode( $atts ) {
	 
	return '<div class="usercentrics-single-consent" consent="'.$atts['consent'].'"></div>';

}

add_shortcode( 'usercentrics-single-consent', 'usercentrics_single_consent_shortcode' );

function usercentrics_single_consent_with_history_shortcode($atts){
	
	return '<div class="usercentrics-single-consent-with-history" consent="'.$atts['consent'].'"></div>';

}
add_shortcode( 'usercentrics-single-consent-with-history', 'usercentrics_single_consent_with_history_shortcode');


function usercentrics_all_consents_with_history_shortcode(){

	return '<div class="usercentrics-all-consents-with-history"></div>';

}
add_shortcode( 'usercentrics-all-consents-with-history', 'usercentrics_all_consents_with_history_shortcode');

function usercentrics_all_consents_shortcode(){

	return '<div class="usercentrics-all-consents"></div>';

}
add_shortcode( 'usercentrics-all-consents', 'usercentrics_all_consents_shortcode');

function usercentrics_select_all_consents_card_shortcode(){
	
	return '<div id="usercentrics-select-all-consents-card"></div>';

}

add_shortcode( 'usercentrics-select-all-consents-card', 'usercentrics_select_all_consents_card_shortcode');

function usercentrics_general_optin_card_shortcode(){

	return '<div id="usercentrics-general-optin-card"></div>';

}

add_shortcode( 'usercentrics-general-optin-card', 'usercentrics_general_optin_card_shortcode');

function usercentrics_modal_shortcode(){

	return '"<a href ="#usercentrics-modal">Open Central Modal</a>"';

}

add_shortcode( 'usercentrics-modal', 'usercentrics_modal_shortcode');

/* Short code end Here */

/*
* Functions for the plugin  
* backend template.
*/


function usercentrics_init(){
	
	global  $wpdb;
	$qry = "SELECT instance,published FROM $wpdb->prefix" . 
	"usercentrics limit 1";
	$usercentrics = $wpdb->get_results( $qry );
	
	if(count($usercentrics)>0){
		$instance_value =	$usercentrics[0]->instance;
		$published      =	$usercentrics[0]->published;
	}
    
	$url  = get_redirect_target('https://api.bitbucket.org/2.0/repositories/usercentrics-docs/content/src');
	$json = file_get_contents($url.'data.json');
	$obj  = json_decode($json);
	
	include( plugin_dir_path( __FILE__ ) . '/include/template.php');
}
?>