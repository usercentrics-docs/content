<?php if($obj==''):?>
<div class="col-md-12 col-sm-12" style="background:#efefef !important;">
  Please connect to internet to make this plugin working. 
</div>
<?php
else:
?>
<div class="col-md-12 col-sm-12" style="background:#efefef !important;">
  <div class="col-md-9 col-sm-8">
  <div style="margin-left:15px;" class="logo col-md-12 col-sm-12" >
  <img src="<?php echo plugins_url( 'wp-usercentrics/images/logo.png'); ?>">
  </div>	
	
  </div>
  <div  class="col-md-3 col-sm-4">
    <div class="checkbox" onclick="myFunction()">
      <label>
        <input id="ios"  
               <?php if($published ==1 ):?>checked
        <?php endif;?> type="checkbox" data-toggle="toggle"  data-on="Enable" data-off="Disabled">
      </label>
    </div>
  </div>
  <div style="background:#efefef !important;min-height:250px;"  class="col-md-12 col-sm-12">
    <div style="border-right: 6px solid #dadada;" class="col-md-7 col-sm-6">
      <p style="margin-top:10px;" class="col-md-12 col-sm-12">
        Usercentres ist eine technische Lösung, um Webseiten & Apps sicher zu machen fur kommende Verscharfungen im Datenchutz.
      </p>
      <div style="" class="headingA col-md-12 col-sm-12">
        <b>
          <?php echo $obj->headingA->label;?>
        </b>
        <span  class="glyphicon glyphicon-info-sign">
        </span>
        <div style="display:none" class="tooltip1 padding">
          <?php echo $obj->headingA->tooltip;?>
        </div>
      </div>
      <?php if (is_admin()) { ?>
      <div style="margin-top:15px;" class="col-md-12 col-sm-12">
        <div class="checkbox disabled">
          <label>
            <input id="toggle-one" disabled checked type="checkbox" data-toggle="toggle"  data-on="<?php echo $obj->toggle->optionLeft->label;?>" data-off="<?php echo $obj->toggle->optionRight->label;?>">
          </label>
        </div>
        <p style="margin-top:15px;">
          <?php echo $obj->toggle->optionLeft->helperText;?>
        </p>
      </div>
      <?php
		}
		else {?>
      <div style="margin-top:15px;" class="col-md-12 col-sm-12">
        <div class="checkbox disabled">
          <label>
            <input id="toggle-one" disabled type="checkbox" data-toggle="toggle"  data-on="<?php echo $obj->toggle->optionLeft->label;?>" data-off="<?php echo $obj->toggle->optionRight->label;?>">
          </label>
        </div>		
        <p style="margin-top:15px;">
          <?php echo $obj->toggle->optionRight->helperText;?>
        </p>
      </div>
      <?php }?>
      <div class="headingB col-md-12 col-sm-12">
        <div>
          <b>
            <?php echo $obj->headingB->label;?>
          </b>
        </div>
        <div class=" headingB_body">
          <div style="background:#<?php echo $obj->headingB->IdEntree->color ?>" class="col-md-12 headingB_top">
            <p class="col-md-12 col-sm-12">
            <div style="color:#fff"class="col-md-3 col-sm-12">
              <?php  echo $obj->headingB->IdEntree->label ?>
            </div>
            <div class="col-md-6 col-sm-12">
              <input class="col-md-12 col-sm-12 settingId" type="textbox" name="settingid" id="settingid" value="<?php echo $instance_value; ?>">
            </div>
            <div class="col-md-2 col-sm-12">
              <button class="settingbutton" onclick="addinstanceid('<?php if($instance_value!=''){ echo "change";}?>');" >
                <?php echo $obj->headingB->IdEntree->buttonLabel?>
              </button>
            </div>
            <div class="col-md-1 col-sm-12" >
              <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                  <a style="border:none;text-decoration:none !important;box-shadow:none" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  </a>
                </h4>
              </div>
            </div>
            </p>
        </div>
        <div class="col-md-12 col-sm-12" style="background:#dadada;margin-top:-15px !important" id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo" role="tabpanel" aria-labelledby="headingTwo">
          <div class="panel-body ">
            <ul class="">
              <li>
                <?php echo $obj->headingB->steps[0];?>
              </li>
              <li>
                <?php echo $obj->headingB->steps[1];?>
              </li>
              <li>
                <?php echo $obj->headingB->steps[2];?>
              </li>
            </ul>
          </div>
        </div>
        <div class="margin col-md-12 col-sm-12" style="background:#<?php if($instance_value !=''){ echo $obj->headingB->cta->withIdEntered->color;}else{ echo $obj->headingB->cta->withoutIdEntered->color; } ?>" class="col-md-12 headingB_top">
          <p class="labelbutton padding">
            <a style="color:#fff" href="<?php if($instance_value !=''){ echo str_replace('{idEnteredByClient}',$instance_value,$obj->headingB->cta->withIdEntered->url); }else{ echo $obj->headingB->cta->withoutIdEntered->url;  } ?>">
              <?php if($instance_value !=''){ echo $obj->headingB->cta->withIdEntered->label;}else{ echo $obj->headingB->cta->withoutIdEntered->label;} ?>  
              <span class="glyphicon glyphicon-play">
              </span>
            </a>
          </p>
        </div>
        <div style="background:#6f6f6f" class="col-md-12 headingB_top3">
          <p >
          <div class="col-md-11 col-sm-11">
            <?php  echo $obj->shortcodes->label; ?>
          </div>
          <div class="col-md-1 col-sm-1">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
                <a style="border:none;text-decoration:none !important;box-shadow:none" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="false" aria-controls="collapsethree">
                </a>
              </h4>
            </div>
          </div>
          </p>
      </div>
      <div style="background:#dadada" id="collapsethree" class="panel-collapse collapse col-md-12" role="tabpanel" aria-labelledby="headingthree" role="tabpanel" aria-labelledby="headingthree">
        <div class="panel-body ">
		
          <ul>
            <li>
              <div class="col-md-6 col-sm-6">
                <?php echo $obj->shortcodes->shortcodes[0]->shorcode;?>
              </div>
              <div class="col-md-6 col-sm-6"> 
                <?php echo $obj->shortcodes->shortcodes[0]->description;?>
              </div>
            </li>
            <li>
              <div class="col-md-6 col-sm-6">
                <?php echo $obj->shortcodes->shortcodes[1]->shorcode;?>
              </div>
              <div class="col-md-6 col-sm-6"> 
                <?php echo $obj->shortcodes->shortcodes[1]->description;?>
              </div>
            </li>
            <li>
              <div class="col-md-6 col-sm-6">
                <?php echo $obj->shortcodes->shortcodes[2]->shorcode;?>
              </div>
              <div class="col-md-6 col-sm-6"> 
                <?php echo $obj->shortcodes->shortcodes[2]->description;?>
              </div>
            </li>
			
			<li>
              <div class="col-md-6 col-sm-6">
                <?php echo $obj->shortcodes->shortcodes[3]->shorcode;?>
              </div>
              <div class="col-md-6 col-sm-6"> 
                <?php echo $obj->shortcodes->shortcodes[3]->description;?>
              </div>
            </li>
            <li>
              <div class="col-md-6 col-sm-6">
                <?php echo $obj->shortcodes->shortcodes[4]->shorcode;?>
              </div>
              <div class="col-md-6 col-sm-6"> 
                <?php echo $obj->shortcodes->shortcodes[4]->description;?>
              </div>
            </li>
            <li>
              <div class="col-md-6 col-sm-6">
                <?php echo $obj->shortcodes->shortcodes[5]->shorcode;?>
              </div>
              <div class="col-md-6 col-sm-6"> 
                <?php echo $obj->shortcodes->shortcodes[5]->description;?>
              </div>
            </li>
			<li>
              <div class="col-md-6 col-sm-6">
                <?php echo $obj->shortcodes->shortcodes[6]->shorcode;?>
              </div>
              <div class="col-md-6 col-sm-6"> 
                <?php echo $obj->shortcodes->shortcodes[6]->description;?>
              </div>
            </li>
            <li>
              <div class="col-md-6 col-sm-6">
                <?php echo $obj->shortcodes->shortcodes[7]->shorcode;?>
              </div>
              <div class="col-md-6 col-sm-6"> 
                <?php echo $obj->shortcodes->shortcodes[7]->description;?>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="headingC row">
      <div class="col-md-12 col-sm-12 padding">
        <b>
          <?php  echo $obj->headingC->label; ?>
        </b>
        <div style="background:#fff" class="col-md-12 headingc_top padding">
          <p class="col-md-12 col-sm-12">
          <div class="col-md-9 col-sm-9">
            <?php  echo $obj->headingC->questions[0]->question; ?>
          </div>
          <div class="col-md-3 col-sm-3">
            <div class="checkbox" onclick="toggle2();">
              <label>
                <input 
                       <?php if($obj->headingC->questions[0]->toggleStatusDefault==''): ?> 
                <?php endif ?> type="checkbox" data-toggle="toggle" data-on="ja" data-off="nein" >
              </label>
            </div>
          </div>
          </p>
        <div id="description1" class="col-md-12 col-sm-12" 
             <?php if($obj->headingC->questions[0]->toggleStatusDefault==1):  else:?> style="display:none" 
        <?php endif ?> class="question1">
        <?php  echo $obj->headingC->questions[0]->description; ?>
      </div>
    </div>
    <div style="background:#fff" class="col-md-12 col-sm-12 headingc_top">
      <p class="col-md-12 col-sm-12">
      <div class="col-md-9 col-sm-9">
        <?php  echo $obj->headingC->questions[1]->question; ?>
      </div>
      <div class="col-md-3 col-sm-3">
        <div class="checkbox-inline " onclick="toggle3();">
          <label>
            <input 
                   <?php if($obj->headingC->questions[1]->toggleStatusDefault=='1'): ?> checked
            <?php endif ?> type="checkbox" id="toggle-two" data-toggle="toggle"  data-on="ja" data-off="nein" >
          </label>
        </div>
      </div>
      </p>
    <div id="description2" class="col-md-12 col-sm-12"  
         <?php if($obj->headingC->questions[1]->toggleStatusDefault=='1'):  else:?> style="display:none" 
    <?php endif ?>  class="question2">
    <?php  echo $obj->headingC->questions[1]->description; ?>
  </div>
</div>
</div>
</div>
</div>
<div style="margin-bottom:30px;" class="disclaimer col-md-12 col-sm-12">
  <?php echo $obj->disclaimer;?>
</div>
</div>
<div class="col-md-5 col-sm-6">
  <div style="margin-top:15px;margin-bottom:15px;" class="col-md-12 col-sm-12">
    <b>
      <?php echo $obj->headingD->label;?>
    </b>
  </div>
  <div class="headingD col-md-12">
    <div class="col-md-12" style="background:#<?php echo $obj->headingD->color ?>" class="col-md-12 headingB_top">
      <p class="col-md-12">
      <div id="time1" style="color:#fff" class="col-md-3">
      </div>
      <div id="time2" class="col-md-3">
      </div>
      <div id="time3" class="col-md-3">
      </div>
      <div id="time4" class="col-md-3">
      </div>
      </p>
    <p class="col-md-12">
    <div  class="col-md-3">TAGE<br/><br/>
    </div>
    <div class="col-md-3">STUNDEN<br/><br/>
    </div>
    <div class="col-md-3">MINUTEN<br/><br/>
    </div>
    <div class="col-md-3">Sekunden<br/><br/>
    </div>
    </p>
</div>
</div>
<div class="headingE col-md-12">
  <div class="padding">
    <b>
      <?php echo $obj->headingE->label ?>
    </b>
  </div>
  <div class="padding">
    <?php echo $obj->headingE->description ?>
  </div>
</div>
<div style="margin-top:15px;margin-bottom:15px;" class="headingF col-md-12 ">
  <div style="background:#fff" class="col-md-12 headingB_top3">
    <p>
    <div style="color:#000" class="col-md-10">
      <?php  echo $obj->FAQ[0]->question; ?>
    </div>
    <div class="col-md-2">
      <div class="panel-heading" role="tab" id="headingfour">
        <h4 class="panel-title">
          <a style="color:blue !important;border:none;text-decoration:none !important;box-shadow:none" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
          </a>
        </h4>
      </div>
    </div>
    </p>
</div>
<div style="background:#fff" id="collapsefour" class="panel-collapse collapse col-md-12" role="tabpanel" aria-labelledby="collapsefour" role="tabpanel" aria-labelledby="collapsefour">
  <div class="panel-body ">
    <?php  echo $obj->FAQ[0]->description; ?>
  </div>
</div>
</div>
<div class="headingG  col-md-12">
  <div style="background:#fff" class="col-md-12  headingB_top3">
    <p>
    <div style="color:#000" class="col-md-10">
      <?php  echo $obj->FAQ[1]->question; ?>
    </div>
    <div class="col-md-2">
      <div class="panel-heading" role="tab" id="headingfive">
        <h4 class="panel-title">
          <a style="color:blue !important;border:none;text-decoration:none !important;box-shadow:none" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
          </a>
        </h4>
      </div>
    </div>
    </p>
</div>
<div style="background:#fff" id="collapsefive" class="panel-collapse collapse col-md-12" role="tabpanel" aria-labelledby="collapsefour" role="tabpanel" aria-labelledby="collapsefives">
  <div class="panel-body ">
    <?php  echo $obj->FAQ[1]->description; ?>
  </div>
</div>
</div>
</div>
</div>
<?php
$protocol = isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://';
$params = array(
'ajaxurl' => admin_url( 'admin-ajax.php', $protocol ),
);
?>	
<script type="text/javascript">
  jQuery('document').ready( function(){
    // Set the date we're counting down to
    var countDownDate = new Date("<?php echo date('M d, Y h:i:s',strtotime($obj->headingD->date)); ?>").getTime();
    // Update the count down every 1 second
    var x = setInterval(function() {
      // Get todays date and time
      var now = new Date().getTime();
      // Find the distance between now an the count down date
      var distance = countDownDate - now;
      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
      // Output the result in an element with id="demo"
      document.getElementById("time1").innerHTML = days;
      document.getElementById("time2").innerHTML = hours;
      document.getElementById("time3").innerHTML = minutes;
      document.getElementById("time4").innerHTML = seconds;
      + "d " + hours + "h "
        + minutes + "m " + seconds + "s ";
      // If the count down is over, write some text 
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
      }
    }
                        , 1000);
  }
                          );
  function toggle3(){
    jQuery('#description2').toggle();
  }
  function toggle2(){
    jQuery('#description1').toggle();
  }
  jQuery('.glyphicon-info-sign').hover( function(){
    jQuery('.tooltip1').toggle(1000);
  }
                                      );
  jQuery('.panel-collapse').on('show.bs.collapse', function () {
    jQuery(this).siblings('.panel-heading').addClass('active');
  }
                              );
  jQuery('.panel-collapse').on('hide.bs.collapse', function () {
    jQuery(this).siblings('.panel-heading').removeClass('active');
  }
                              );
  function addinstanceid(task){
    var settingid = jQuery("#settingid").val();
    if(task=='change'){
      if(settingid==''){
        alert('Please enter valid Instance ID');
      }
      else{
        jQuery.ajax({
          url : '<?php echo $params['ajaxurl'];?>',
          type : 'POST',
          data :{
          "action" : 'save_setting_ID',
          "settingId" : settingid,
          "task"       :'change',
          "security" :'<?php echo wp_create_nonce( 'my-nonce' );?>'
        }
                    ,
                    success : function( response ) {
          alert("Instance is  updated Successfully");
          location.reload();
        }
      }
      );
    }
  }
  else{
    if(settingid==''){
      alert('Please enter valid Instance ID');
    }
    else{
      jQuery.ajax({
        url : '<?php echo $params['ajaxurl'];?>',
        type : 'POST',
        data :{
        "action" : 'save_setting_ID',
        "settingId" : settingid,
        "security" :'<?php echo wp_create_nonce( 'my-nonce' );?>'
      }
                  ,
                  success : function( response ) {
        alert("Instance is set up Successfully");
        location.reload();
      }
    }
    );
  }
  }
  }
  function myFunction(){
    //set initial state.
    var checkBox = document.getElementById("ios");
    var status;
    if(checkBox.checked == true) {
      status =0;
    }
    else{
      status =1;
    }
    jQuery.ajax({
      url : '<?php echo $params['ajaxurl'];?>',
      type : 'POST',
      data :{
      "action" : 'save_setting_status',
      "status" : status,
      "security" :'<?php echo wp_create_nonce( 'my-nonce' );?>'
    }
                ,
                success : function( response ) {
      alert(response);
    }
  }
  );
  }
</script>
<?php endif; ?>